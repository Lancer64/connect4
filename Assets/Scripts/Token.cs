﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token
{
    string position;//coordenada XY
    private Token top;
    private Token down;
    private Token right;
    private Token left;
    private bool onTop;
    private int color; // 0 blanco, 1 rojo, azul 2

    public Token(string position, int color)
    {
        this.color = color;
        this.position = position;
        this.top = null;
        this.right = null;
        this.down = null;
        this.left = null;
        this.onTop = false;
    }


    //Checa cuantos nodos consecutivos a la derecha comparten el mismo valor.
    public int CheckRight(int wantedValue)
    {
        int suma = 0;
        if (right is null || right.color == 0)
        {
            return 0;
        }
        else if (right.color == wantedValue)
        {
            suma = right.CheckRight(wantedValue) + 1;
        }

        return suma;
    }

    //Checa cuantos nodos consecutivos a la izquierda comparten el mismo valor.
    public int CheckLeft(int wantedValue)
    {
        int suma = 0;
        if (left is null || left.color == 0)
        {
            return 0;
        }
        else if (left.color == wantedValue)
        {
            suma = left.CheckLeft(wantedValue) + 1;
        }

        return suma;
    }

    //Checa cuantos nodos consecutivos arriba comparten el mismo valor.
    public int CheckTop(int wantedValue)
    {
        int suma = 0;
        if (top is null || top.color == 0)
        {
            return 0;
        }
        else if (top.color == wantedValue)
        {
            suma = top.CheckTop(wantedValue) + 1;
        }

        return suma;
    }

    //Checa cuantos nodos consecutivos abajo comparten el mismo valor.
    public int CheckDown(int wantedValue)
    {
        int suma = 0;
        if (down is null || down.color == 0)
        {
            return 0;
        }
        else if (down.color == wantedValue)
        {
            suma = down.CheckDown(wantedValue) + 1;
        }

        return suma;
    }

    //Checa cuantos nodos consecutivos en la diagonal derecha superior comparten el mismo valor.
    public int CheckTopRight(int wantedValue)
    {
        int suma = 0;
        if (top is null || top.right is null || top.right.color == 0)
        {
            return 0;
        }
        else if (top.right.color == wantedValue)
        {
            suma = top.right.CheckTopRight(wantedValue) + 1;
        }

        return suma;
    }

    //Checa cuantos nodos consecutivos en la diagonal izquierda superior comparten el mismo valor.
    public int CheckTopLeft(int wantedValue)
    {
        int suma = 0;
        if (top is null || top.left is null || top.left.color == 0)
        {
            return 0;
        }
        else if (top.left.color == wantedValue)
        {
            suma = top.left.CheckTopLeft(wantedValue) + 1;
        }

        return suma;
    }

    //Checa cuantos nodos consecutivos en la diagonal izquierda inferior comparten el mismo valor.
    public int CheckDownLeft(int wantedValue)
    {
        int suma = 0;
        if (down is null || down.left is null || down.left.color == 0)
        {
            return 0;
        }
        else if (down.left.color == wantedValue)
        {
            suma = down.left.CheckDownLeft(wantedValue) + 1;
        }

        return suma;
    }

    //Checa cuantos nodos consecutivos en la diagonal derecha inferior comparten el mismo valor.
    public int CheckDownRight(int wantedValue)
    {
        int suma = 0;
        if (down is null || down.right is null || down.right.color == 0)
        {
            return 0;
        }
        else if (down.right.color == wantedValue)
        {
            suma = down.right.CheckDownRight(wantedValue) + 1;
        }

        return suma;
    }

    //Encuentra el primer nodo sin valor (rojo/azul) asignado
    public int FindFirstAvailable(int start)
    {
        int row = start;
        if(down != null && down.color == 0)
        {
            row++;
            row = down.FindFirstAvailable(row);
        }
        return row;
    }

    public Token Top { get => top; set => top = value; }
    public Token Down { get => down; set => down = value; }
    public Token Right { get => right; set => right = value; }
    public Token Left { get => left; set => left = value; }
    public int Color { get => color; set => color = value; }
    public string Position { get => position; set => position = value; }
    public bool OnTop { get => onTop; set => onTop = value; }
}
