﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    int index; //where the topMarker is located.
    public Button topMarker; //object that allow us to choose where the next token will be placed
    List<Token> tokens;
    int playerColor;
    public Sprite spriteRed;
    public Sprite spriteBlue;
    public Sprite spriteWhite;
    private bool gameOver;
    public Text winnerText;

    void Start()
    {
        tokens = new List<Token>();
        SetTokens();
        Restart();
    }


    //Reinicia el juego
    public void Restart()
    {
        gameOver = true;
        StopCoroutine(PlayBlue());
        winnerText.gameObject.SetActive(false); 
        
        playerColor = 1; //EL jugador rojo es siempre el primero en empezar
        index = 0;

        topMarker.GetComponent<Image>().sprite = spriteRed;
        //Maecador que sirve para que el jugador decida donde colocar la siguiente ficha/token
        topMarker.transform.localPosition = new Vector2(-106.0f, 129.0f);
        TurnButtonsWhite();
        gameOver = false;
    }

    //Crea a todos los tokens del tablero y define sus relaciones entre sí
    private void SetTokens()
    {
        //Crea todos los nods y define su posición
        for (int i = 0; i < 42; i++)
        {
            string position = "" + i / 7 + "" + i % 7;
            tokens.Add(new Token(position, 0));
        }
        //Define las relaciones entre entre los nodos.
        for (int i = 0; i < 42; i++)
        {
            Token currentToken = tokens[i];
            if (i > 6) currentToken.Top = tokens[i - 7];
            if (i < 35) currentToken.Down = tokens[i + 7];
            if ((i % 7) > 0)
            {
                currentToken.Left = tokens[i - 1];
            }
            else
            {
                currentToken.OnTop = true;
            }
            if ((i % 7) < 6) currentToken.Right = tokens[i + 1];
        }
    }

    //Pinta a los tokens de blanco
    void TurnButtonsWhite()
    {
        for(int i = 0; i < 42; i++)
        {
            tokens[i].Color = 0;
            int indexRow = i / 7;
            int indexColumn = i % 7;
            GameObject rowCanvas = GameObject.Find("Canvas").transform.GetChild(indexRow + 2).gameObject;
            GameObject buttonRow = rowCanvas.transform.GetChild(indexColumn).gameObject;
            buttonRow.GetComponent<Image>().sprite = spriteWhite;
        }
    }

    //Imprime los nodos vecinos del nodo seleccionado
    public void CheckNeighbors(int position)
    {
        Token tokenRequested = tokens[position];
        //" tokenUp:" + tokenRequested.Top.Position + 
        Debug.Log(" tokenDown:" + tokenRequested.Down.Position +
            " tokenRight:" + tokenRequested.Right.Position + " tokenLeft:" + tokenRequested.Left.Position);
    }

    //Cuenta si ya se llenaron todos los tokens con las fichas de los jugadores.
    bool CountTie()
    {
        for(int i = 0; i < 7; i++)
        {
            if(tokens[i].Color == 0)
            {
                return false;
            }
        }
        return true;
    }

    void Update()
    {
        if(Input.GetKeyDown("r"))
        {
            Restart();
        }
        //SI el juego termine, no se leen más comandos de ningún jugador
        if (gameOver || playerColor == 2) return;

        //para mover el marcador
        if (Input.GetKeyDown("right"))
        {
            if (index < 6)
            {
                index++;
                topMarker.transform.Translate(53, 0, 0);
            }
        }
        else if (Input.GetKeyDown("left"))
        {
            if (index > 0)
            {
                index--;
                topMarker.transform.Translate(-53, 0, 0);
            }
        }//Para decidir donde colocar la ficha.
        else if (Input.GetKeyDown("down"))
        {
            if(PlayToken())
              StartCoroutine(PlayBlue());
        }
    }


    //Jugada del jugador azul (CPU)
    IEnumerator PlayBlue()
    {
        int blueChoice = RandomAI();
        index = blueChoice;
        int topMarketXPosition = -106 + (index * 53);
        topMarker.transform.localPosition = new Vector2(topMarketXPosition, 129.0f);
        yield return new WaitForSeconds(1.0f);
        if(!gameOver)
            PlayToken();
    }

    //Se coloca la ficha elegida por el jugador 
    bool PlayToken()
    {
        if (gameOver) return false;
        if(CountTie())
        {
            gameOver = true;
            ShowTie ();
            return false;
        }

        Token topToken = tokens[index];
        int indexRow = topToken.FindFirstAvailable(0);
        int indexSelectToken = index + 7 * indexRow;

        //Checa si hay espacio disponible en la columna seleccionada.
        if (topToken.Color == 0)
        {
            Token selectToken = tokens[indexSelectToken];
            selectToken.Color = playerColor;

            GameObject rowCanvas = GameObject.Find("Canvas").transform.GetChild(indexRow + 2).gameObject;
            GameObject buttonRow = rowCanvas.transform.GetChild(index).gameObject;
            if (playerColor == 1)
            {
                buttonRow.GetComponent<Image>().sprite = spriteRed;
            }
            else
            {
                buttonRow.GetComponent<Image>().sprite = spriteBlue;
            }

            if (CheckWinner(indexSelectToken))
            {
                ShowTextWinner();
                gameOver = true;
                return false;
            }
            ChangePlayerColor();
            return true;
        }
        return false;
    }

    //Se elige aleatoriamente la siguiente jugada del jugador, mientras aun hay espacio disponible
    //en esa columna
    int RandomAI()
    {
        int randomNumber = (int) Random.Range(0.0f, 6.0f);
        Token topToken = tokens[randomNumber];
        while(topToken.Color != 0)
        {
            randomNumber = (int)Random.Range(0.0f, 6.0f);
            topToken = tokens[randomNumber];
        }
        return randomNumber;
    }

    //Cambia el turno del jugador y el color de la ficha que se va a colocar.
    void ChangePlayerColor()
    {
        playerColor = (playerColor == 1) ? 2 : 1 ;
        if(playerColor == 1)
        {
            topMarker.GetComponent<Image>().sprite = spriteRed;
        }
        else
        {
            topMarker.GetComponent<Image>().sprite = spriteBlue;
        }
    }

    //Checa si el jugador ganó con su última jugada.
    bool CheckWinner(int position)
    {
        Token selectToken = tokens[position];
        //Si en alguno de los lados de la ficha encuentra que hay al menos 3 fichas 
        //con el mismo valor, entonces el jugador ganó.
        int selectColor = selectToken.Color;
        int horizontalWinner = selectToken.CheckRight(selectColor) + selectToken.CheckLeft(selectColor);
        if (horizontalWinner >= 3) return true;

        int verticallWinner = selectToken.CheckTop(selectColor) + selectToken.CheckDown(selectColor);
        if (verticallWinner >= 3) return true;

        int diaglonalWinner1 = selectToken.CheckTopLeft(selectColor) + selectToken.CheckDownRight(selectColor);
        if (diaglonalWinner1 >= 3) return true;

        int diaglonalWinner2 = selectToken.CheckTopRight(selectColor) + selectToken.CheckDownLeft(selectColor);
        if (diaglonalWinner2 >= 3) return true;

        return false;
    }

    void ShowTie()
    {
        winnerText.text = "Tie!\n No Winner.";
        winnerText.gameObject.SetActive(true);
    }

    //Muestra el jugador ganador.
    void ShowTextWinner()
    {
        string winnerString = (playerColor == 1) ? "Red " : "Blue " ;
        winnerText.text = winnerString + "is the Winner!!!";
        winnerText.gameObject.SetActive(true);
    }
}
